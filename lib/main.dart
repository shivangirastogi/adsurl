import 'package:ads_url/feature/authentication/presentation/screen/forget_password_screen.dart';
import 'package:ads_url/feature/authentication/presentation/screen/log_in_screen.dart';
import 'package:ads_url/feature/select_language/presentation/select_language.dart';
import 'package:flutter/material.dart';

import 'feature/authentication/presentation/views/app_button.dart';
import 'feature/authentication/presentation/views/app_button_with_icon.dart';
import 'feature/authentication/presentation/views/app_text_field.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: SelectLanguageScreen(),
    );
  }
}

