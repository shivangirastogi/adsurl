import 'package:ads_url/core/app_color.dart';
import 'package:ads_url/feature/authentication/presentation/screen/log_in_screen.dart';
import 'package:ads_url/feature/authentication/presentation/views/app_button_with_icon.dart';
import 'package:ads_url/generated/l10n.dart';
import 'package:flutter/material.dart';

class SelectLanguageScreen extends StatefulWidget {
  @override
  _SelectLanguageScreenState createState() => _SelectLanguageScreenState();
}

class _SelectLanguageScreenState extends State<SelectLanguageScreen> {
  S locale= new S();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
              children: <Widget>[
                SizedBox(height:150,),

             Text('Welcome', style: TextStyle(
               fontSize: 30,
               fontWeight: FontWeight.bold,
             ),),
                SizedBox(height:130,),
                Container(width: 420.0,
                  height: 230.0,
                  padding: new EdgeInsets.all(18),
                  margin: new EdgeInsets.all(18),
                  decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(18),
                      color: AppColors.white,
                      boxShadow: [
                        new BoxShadow(
                          color: Colors.black12,
                          blurRadius: 8.0,
                        ),
                      ]),
                  child: Column(

                    children:<Widget> [

                      Text(locale.selectLanguage, style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                      ),),
                      SizedBox(height: 30,),
                      AppButtonWithIcon(title: locale.arabic, imagePath: 'assets/ar.png', color: AppColors.buttoniconColor, onPress: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => LogInScreen()),
                        );
                      }, textColor: AppColors.white,),
                      SizedBox(height: 10,),
                      AppButtonWithIcon(title: locale.english,  imagePath:'assets/en.jpeg', color: AppColors.white, onPress: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => LogInScreen()),
                        );
                      }, textColor: AppColors.black,),


                    ],
                  ),


                ),
          ],
        ),
      ),


    );
  }
}
