import 'package:ads_url/core/app_color.dart';
import 'package:ads_url/feature/authentication/presentation/views/app_button.dart';
import 'package:ads_url/feature/authentication/presentation/views/app_text_field.dart';
import 'package:ads_url/feature/authentication/presentation/views/text_button.dart';
import 'package:ads_url/feature/dashboard/presentation/screen/product_screen.dart';
import 'package:ads_url/generated/l10n.dart';
import 'package:flutter/material.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();
  S locale = new S();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          SizedBox(
            height: 32,
          ),
          Text(
            locale.welcome,
            style: TextStyle(
              color: AppColors.black,
              fontSize: 28,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            width: 280,
            child: Text(
              locale.sign_up_to,
              style: TextStyle(
                color: AppColors.black,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Form(
            key: _formKey,
            child: Container(
              width: 450.0,
              height: 350.0,
              padding: new EdgeInsets.all(20),
              margin: new EdgeInsets.all(18),
              decoration: new BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                  color: AppColors.white,
                  boxShadow: [
                    new BoxShadow(
                      color: Colors.black12,
                      blurRadius: 8.0,
                    ),
                  ]),

              child: Column(

                children: <Widget>[
                  Text(locale.sign_up),
                  AppTextField(
                      hint: 'Username',
                      validation: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      hidden: false),
                  AppTextField(
                      hint: 'Email Id',
                      validation: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                    hidden: false,),
                  SizedBox(height: 70,),
                  AppButton(
                      title: locale.signup,
                      onPress: () {
                        if (_formKey.currentState.validate())
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProductScreen()),
                          );
                      })
                ],
              ),
            ),
          ),
          TextButtonScreen(
              textButton: locale.continueAsGuest,
              onPress: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProductScreen ()),
                );
              }),
          Text(locale.or),
          SizedBox(
            width: 200,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 30,
                  child: Image.network(
                      "https://cdn.iconscout.com/icon/free/png-256/instagram-1868978-1583142.png"),
                ),
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 30,
                  child: Image.network(
                      "https://image.flaticon.com/icons/png/512/124/124010.png"),
                ),
              ],
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SignUpScreen()),
              );
            },
            child: Text(locale.dont_have),
          ),
        ],
      ),
    );
  }
}
