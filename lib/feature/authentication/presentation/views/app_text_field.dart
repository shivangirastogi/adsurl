import 'package:flutter/material.dart';

class AppTextField extends StatelessWidget {
  final String hint;
  final FormFieldValidator validation;
  final bool hidden;

  const AppTextField({Key key,@required this.hint,@required this.validation,@required this.hidden}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return TextFormField(
      obscureText: hidden,
      decoration: InputDecoration(
        labelText: hint,
      ),
      validator: validation,


    );
  }
}
