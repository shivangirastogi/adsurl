import 'package:ads_url/core/app_color.dart';
import 'package:flutter/material.dart';

class AppButtonWithIcon extends StatelessWidget {
  final String title;
  final String imagePath;
  final Color color;
  final Function onPress;
  final Color textColor;

  const AppButtonWithIcon(
      {Key key,
      @required this.title,
      @required this.imagePath,
      @required this.color,
      @required this.onPress,
      @required this.textColor})
      : super(key: key);

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return AppColors.buttoniconColor;
    }
    return AppColors.white;
  }

  Color getBgColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return AppColors.buttoniconColor;
    }
    return color;
  }

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
          foregroundColor: MaterialStateProperty.resolveWith(getColor),
          backgroundColor: MaterialStateProperty.resolveWith(getBgColor),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(32.0),
            side: BorderSide(color: AppColors.buttonOutline),
          )),
          minimumSize: MaterialStateProperty.all<Size>(Size(
            double.infinity,
            48,
          ))),
      child: Row(
        children: [
          SizedBox(
            width: 30,
          ),
          Text(
            title,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: textColor,
            ),
          ),
          Spacer(),
          SizedBox(height: 30, child: Image.asset(imagePath)),
          SizedBox(
            width: 30,
          ),
        ],
      ),
      onPressed: onPress,
    );
  }
}
