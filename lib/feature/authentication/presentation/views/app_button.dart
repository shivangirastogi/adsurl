import 'package:ads_url/core/app_color.dart';
import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  final String title;
  final Function onPress;

  AppButton({Key key,@required this.title,@required this.onPress}) : super(key: key);


 Color getFgColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return AppColors.buttoniconColor;
    }
    return AppColors.white;
  }
  Color getBgColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return AppColors.buttoniconColor;
    }
    return AppColors.buttoniconColor;
  }


  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.resolveWith(getFgColor),
        backgroundColor: MaterialStateProperty.resolveWith(getBgColor),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(32.0),
                  side: BorderSide(color: AppColors.buttonOutline),
              )
          ),
          minimumSize: MaterialStateProperty.all<Size>(
          Size(
            double.infinity,
            48,

          )
      )


      ),
      child:  Text(title,
        style: TextStyle(
          fontSize: 18,
          color: AppColors.white,
        ),
        textAlign: TextAlign.center,),
      onPressed:onPress,

    );
  }
}
