import 'package:ads_url/generated/l10n.dart';
import 'package:flutter/material.dart';

class TextButtonScreen extends StatelessWidget {
  final String textButton;
  final Function onPress;


  TextButtonScreen({Key key, @required this.textButton, @required this.onPress})
      : super(key: key);
  S locale = new S();

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPress,
      child: Text(textButton),
    );
  }
}
