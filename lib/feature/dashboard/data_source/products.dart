import 'dart:core';

import 'package:ads_url/feature/dashboard/model/product.dart';

class Products{
  static List<ProductModel> products = [
    ProductModel(image: "", price: "123", title: "jumpsuit"),
    ProductModel(image: "", price: "456", title: "jumpsuit1"),
    ProductModel(image: "", price: "111", title: "jumpsuit2"),
    ProductModel(image: "", price: "999", title: "jumpsuit3"),
    ProductModel(image: "", price: "565", title: "jumpsuit4"),
    ProductModel(image: "", price: "456", title: "jumpsuit5"),
    ProductModel(image: "", price: "1235", title: "jumpsuit6"),
    ProductModel(image: "", price: "456", title: "jumpsuit7"),
  ]  ;
}