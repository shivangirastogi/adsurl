
import 'package:flutter/material.dart';

class ProductModel{
  final String image;
  final String price;
  final  String title;

  ProductModel({@required this.image,@required this.price,@required this.title});


}