import 'package:ads_url/core/app_color.dart';
import 'package:ads_url/feature/dashboard/presentation/view/product_gridView.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class ProductScreen extends StatefulWidget {
  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {


  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static  List<Widget> _widgetOptions = <Widget>[



    ProductGridScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text('LUSIVE'
          ),
        ),
        actions: [
          IconButton(icon: Icon(Icons.favorite_border, color: AppColors.white,), onPressed: null),
          IconButton(icon: Icon(Icons.search,color: AppColors.white,), onPressed: null),

        ],
      ),
      body: Column(
        children: [
          CarouselSlider(
              items: [
                Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 16,
                  ),
                  height: 150,
                  width: 350,
                  child: Center(
                    child: Text('Promo Text1',style: TextStyle(
                      fontSize: 32,
                      color: AppColors.white,
                    ),),
                  ),
                  decoration: BoxDecoration(
                    color: AppColors.appbarText,
                    borderRadius: BorderRadius.circular(32),
                  ),
                ),
                Container(
                  height: 150,
                  width: 350,
                  margin: EdgeInsets.symmetric(
                    vertical: 16,
                  ),
                  child: Center(
                    child: Text('Promo Text2',style: TextStyle(
                      fontSize: 32,
                      color: AppColors.white,
                    ),),
                  ),
                  decoration: BoxDecoration(
                    color: AppColors.appbarText,
                    borderRadius: BorderRadius.circular(32),
                  ),
                ),
              ],
              options: CarouselOptions(
                height: 150,
                aspectRatio: 16/9,
                viewportFraction: 0.8,
                initialPage: 0,
                enableInfiniteScroll: true,
                reverse: false,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                enlargeCenterPage: true,
                scrollDirection: Axis.horizontal,
              )
          ),
          Expanded(child: _widgetOptions.elementAt(_selectedIndex)),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
          showUnselectedLabels: false,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: Colors.blueGrey,),
            label: 'Home',
            backgroundColor: AppColors.backgroundColor,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business,color: Colors.blueGrey),
            label: 'Business',
            backgroundColor: AppColors.backgroundColor,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school,color: Colors.blueGrey),
            label: 'School',
            backgroundColor: AppColors.backgroundColor,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings,color: Colors.blueGrey),
            label: 'Settings',
            backgroundColor: AppColors.backgroundColor,
          ),
        ],
      ),
    );
  }
}
