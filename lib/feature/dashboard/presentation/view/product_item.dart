import 'package:ads_url/core/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return   Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: double.maxFinite,
            height: 200,
            padding: const EdgeInsets.all(8),
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.topRight,
                  child: Icon(
                    Icons.favorite_border,
                    color: AppColors.white,
                  ),
                )
              ],
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18),
              image: DecorationImage(
                image: AssetImage("assets/image.jpeg"),
              ),
            ),
          ),
          Text('AED 399  60% Off'),
          Text('LUSIVE Jumpsuit'),

        ],
      ),
    );
  }
}
