import 'package:ads_url/core/app_color.dart';
import 'package:ads_url/feature/dashboard/presentation/view/product_item.dart';
import 'package:flutter/material.dart';

class ProductGridScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: GridView.count(
          childAspectRatio: 0.8,
      primary: false,
      padding: const EdgeInsets.all(20),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      crossAxisCount: 2,
      children: <Widget>[
       ProductItem(),
       ProductItem(),
       ProductItem(),
       ProductItem(),
       ProductItem(),
       ProductItem(),
       ProductItem(),
       ProductItem(),
       ProductItem(),
       ProductItem(),
      ],
    ));
  }
}
