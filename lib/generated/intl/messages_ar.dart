// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ar locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ar';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "arabic" : MessageLookupByLibrary.simpleMessage("Arabic  "),
    "continueAsGuest" : MessageLookupByLibrary.simpleMessage("Continue as Guest >"),
    "dont_have" : MessageLookupByLibrary.simpleMessage("Don\'t have an account?"),
    "english" : MessageLookupByLibrary.simpleMessage("English"),
    "forgetPassword" : MessageLookupByLibrary.simpleMessage("Forget Password ?"),
    "login" : MessageLookupByLibrary.simpleMessage("LOGIN"),
    "login_up" : MessageLookupByLibrary.simpleMessage("Log In"),
    "or" : MessageLookupByLibrary.simpleMessage("-OR-"),
    "selectLanguage" : MessageLookupByLibrary.simpleMessage("Select Language (ar)"),
    "sign_up" : MessageLookupByLibrary.simpleMessage("Sign Up"),
    "sign_up_to" : MessageLookupByLibrary.simpleMessage("Sign up to get started and experience great shopping deals"),
    "signup" : MessageLookupByLibrary.simpleMessage("SIGN UP"),
    "welcome" : MessageLookupByLibrary.simpleMessage("Welcome ar")
  };
}
