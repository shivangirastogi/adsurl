
import 'package:flutter/material.dart';

class AppColors {
  AppColors._();


  static const buttoniconColor = const Color.fromRGBO(24, 43, 49, 1);
  static const backgroundColor = const Color.fromRGBO(247, 248, 242, 1);
  static const cardOutline = const Color.fromRGBO(245, 244, 242, 1);
  static const white = Colors.white;
  static const black = Colors.black;
  static const userName = const Color.fromRGBO(153, 153,153, 1);
  static const line_Color = const Color.fromRGBO(216, 216, 216, 1);
  static const fpColor = const Color.fromRGBO(10, 10, 10, 1);
  static const buttonOutline = const Color.fromRGBO(152, 160, 163, 1);
  static const textColor= const Color.fromRGBO(62, 62,62, 1);
  static const orColour= const Color.fromRGBO(101, 101, 101, 1);
  static const cagColor = const Color.fromRGBO(45, 45, 45, 1);
  static const textColor2= const Color.fromRGBO(56, 56,  56, 1);
  static const bottomColor= const Color.fromRGBO(209, 208,206, 1);
  static const appbarText = const Color.fromRGBO( 31, 59, 64, 1);
  static const upperCont= const Color.fromRGBO(35, 129,  142, 1);
  static const slideColor= const Color.fromRGBO( 130, 180, 186, 1);
  static const seemoreColor= const Color.fromRGBO( 128, 134, 149, 1);
}
